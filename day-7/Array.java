import java.util.Arrays;

public class Array {
    public static void main(String[] args) {
        int[] arr = {3,44,56,234,12,34,54};

        // how to declare arrays
        int[] arr1;
        int arr2[];
        
        // array cannot be changed the its length
        int[] arr3;
        arr3 = new int[]{67,432,543,653,123};
        
        // the array before sorted
        System.out.println("Array before sorted");
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println("\n");

        // sort the array
        Arrays.sort(arr);

        // the array after sorted
        System.out.println("Array after sorted");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("\n");

        // foreach looping for print out the array elements
        System.out.println("foreach looping");
        for ( int num : arr3) {
            System.out.print(num + " ");
        }
        System.out.println("\n");

        // common for looping
        System.out.println("common for looping");
        for(int i = 0; i < arr3.length; i++) {
            System.out.print(arr3[i] + " ");
        }
        System.out.println();
    }    
}
