package com.dodo

public class Vehicles{
    private int wheels;
    private String brand;
    private int speed;

    public Vehicles(int wheels, String brand, int speed) {
        this.wheels = wheels;
        this.brand = brand;
        this.speed = speed;
    }

    public void getWheels() {
        return this.wheels;
    }

    public int setWheels(int wheels){
        return this.wheels = wheels;
    }

    public void getBrand() {
        return this.brand;
    }

    public String setBrand(String brand) {
        return this.brand = brand
    }

    public void getSpeed() {
        return this.speed;
    }

    public int setSpeed(int speed){
        return this.speed = speed;
    }

}