public class Ternery {
    public static void main(String[] args) {
        int randomNumber = (int) (1 + Math.random() * 10);

        String str = (randomNumber < 5) ? "Number is lower than 5" : ((randomNumber > 5) ? "Number is higher than 5" : "Number is 5");

        System.out.println(str + "\nRandom number is " + randomNumber);
    }    
}
