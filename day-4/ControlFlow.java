public class ControlFlow {
    public static void main(String[] args) {
        int randomNumber = (int) (1 + Math.random() * 10);

        if(randomNumber > 5) {
            System.out.println("Number is bigger than 5");
        } else if (randomNumber < 5) {
            System.out.println("Number is lower than 5");
        } else {
            System.out.println("Number is 5");
        }

        System.out.println("The random number is " + randomNumber);
    }
}
