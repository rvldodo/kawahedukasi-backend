import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Collection {
    public static void main(String[] args) {
        System.out.println("COLLECTION");
        System.out.println();
        /**
         * ArrayList
         * -> similar to the normal Array but with ArrayList you can delete and add value inside the ArrayList
         * -> but thw normal Array cannot do it
         * -> that means ArrayList is more dynamic than normal Array
         * -> also with ArrayList you can add different type of values such as(int, string, boolean, float, double) in one big array
         * -> but the normal Array cannot do it
         */
        System.out.println("ArrayList");
        ArrayList<Object> randomArr = new ArrayList<>();
        randomArr.add("fizz");
        randomArr.add(20);
        randomArr.add("2adf4");

        // printout the array
        System.out.println(randomArr);
        // change the value by index
        randomArr.set(1, "buzz");
        System.out.println(randomArr);
        // remove the value inside the array
        randomArr.remove(0);
        // print the value inside the array by index
        System.out.println(randomArr.get(1));
        // get the size of the array
        System.out.println(randomArr.size());
        // delete all the value inside the array
        randomArr.clear();
        System.out.println(randomArr);

        System.out.println();

        /*
         * HashSet
         * -> similar to ArrayList except you can only add unique values, that means the value inside this array cannot be twice
         * -> and in this type of collection there is method where you can check is there is a value inside the array
         */
        System.out.println("HashSet");
        HashSet<String> cars = new HashSet<String>();
        cars.add("Lamborgini");
        cars.add("Mazda");
        cars.add("BMW");
        cars.add("Mazda");

        // printout the array
        System.out.println(cars);
        // print true or false wether there is value inside the array or not
        System.out.println(cars.contains("Mazda"));
        System.out.println("Toyota");
        // get the size of the array
        System.out.println(cars.size());
        // delete all the value inside the array
        cars.clear();
        System.out.println(cars);

        System.out.println();

        /**
         * HashMap
         * -> in this collection we must add 2 value, the first value is the key value and the second value is the real value
         * -> and when we want to get the value from this collection we need to use .get() method and enter the key value
         * -> also in this collection we use .put() method to input th values not .add() method like other collections
         */
        System.out.println("HashMap");
        HashMap<String, Integer> myArray = new HashMap<String, Integer>();
        // add the value to the array
        myArray.put("Toyota", 4);
        myArray.put("Truck", 6);

        // print the array
        System.out.println(myArray);
        // printout the array by the key value not index
        System.out.println(myArray.get("Toyota"));
        // print the size of the array
        System.out.println(myArray.size());
        // looping
        for (String keyArr : myArray.keySet()) {
            System.out.print(keyArr + " "); // print the key from the array
        }
        System.out.println();

        for (Integer valueArr : myArray.values()) {
            System.out.print(valueArr + " "); // print the value from the array
        }
        System.out.println();

        System.out.println();

        // test
        System.out.println("Testing HashMap");
        HashMap<String, Object> array = new HashMap<String, Object>();
        array.put("1", myArray);

        System.out.println(array);
    }
}
