public class Method {
    public static int penjumlahan(int a, int b) {
        return a + b;
    }

    public static int factorial(int a) {
        if(a <= 2) return a;

        return a * factorial(a - 1);
    }

    public static void main(String[] args) {
        int fact = factorial(penjumlahan(2,3));

        System.out.println(fact);
    }
}
