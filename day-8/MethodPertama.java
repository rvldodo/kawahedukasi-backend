
interface Method1 {
	
	String methodA(String str);

}

public class MethodPertama {

	public static void main(String[] args) {
	
		Method1 coba = (kata) -> kata + " " + kata;
		cetak(coba, "word");
	
	}

	public static void cetak(Method1 method, String stringA) {
	
		String percobaan = method.methodA(stringA);
		
		System.out.println(percobaan);
	
	}
}


