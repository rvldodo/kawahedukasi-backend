public class Kendaraan {
    private int jumlahRoda;
    private String merkKendaraan;
    private String platNomor;
    private int kecepatan;

    public Kendaraan(int jumlahRoda, String merkKendaraan, String platNomor) {
        this.jumlahRoda = jumlahRoda;
        this.merkKendaraan = merkKendaraan;
        this.platNomor = platNomor;
    }

    public int getJumlahRoda() {
        return jumlahRoda;
    }

    public void setJumlahRoda(int jumlahRoda) {
        this.jumlahRoda = jumlahRoda;
    }

    public String getMerkKendaraan() {
        return merkKendaraan;
    }

    public void setMerkKendaraan(String merkKendaraan) {
        this.merkKendaraan = merkKendaraan;
    }

    public String getPlatNomor() {
        return platNomor;
    }

    public void setPlatNomor(String platNomor) {
        this.platNomor = platNomor;
    }

    public int getKecepatan() {
        return kecepatan;
    }

    public void setKecepatan(int kecepatan) {
        this.kecepatan = kecepatan;
    }

    @Override
    public String toString() {
        return "Kendaraan beroda " + getJumlahRoda() + " bermerek " + getMerkKendaraan() + " bernomor " + getPlatNomor() + " dan berkecepatan " + this.kecepatan + " km/h";
    }

    public int speedUp(int speed) {
        return this.kecepatan += speed;
    }
}
