import java.util.Scanner;

public class Looping {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter input: ");
        int input = scanner.nextInt();
        scanner.nextLine();

        for(int i=0; i<input; i++) {
            for(int j=0; j<=i; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
