package com.dodo.objects;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Fruit {
    @Schema(required = true, example = "Orange")
    @NotBlank(message = "Name cannot be empty")
    private String name;
    @Schema(required = true, example = "Orange")
    @NotBlank(message = "Color cannot be empty")
    private String color;
    @Schema(required = true, example = "3")
    @Min(message = "Cannot be negative", value = 0)
    @NotNull(message = "Total cannot be empty")
    private Integer total;

    public Fruit() {
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Fruit(String name, String color, Integer total) {
        this.name = name;
        this.color = color;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
