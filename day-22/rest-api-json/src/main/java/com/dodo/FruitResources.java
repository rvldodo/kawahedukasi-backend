package com.dodo;

import com.dodo.objects.Fruit;

import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Path("/fruits")
public class FruitResources {

    @Inject
    Validator validator;

    List<Fruit> fruitList = new ArrayList<>();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllFruits(){
        return Response.ok(fruitList).build();
    }

    //get a fruit by index
    @GET
    @Path("{index}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFruit(@PathParam("index") Integer index){
        Fruit fruit = fruitList.get(index);
        return Response.ok(fruit).build();
    }

    // create a fruit or add a fruit
    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result createFruit(Fruit fruit){
        Set<ConstraintViolation<Fruit>> violations = validator.validate(fruit);
        if(violations.isEmpty()) {
            fruitList.add(fruit);
            return new Result("Success add data");
        } else {
            return new Result(violations.stream().map(cv -> cv.getMessage()).collect(Collectors.joining(", ")));
        }
    }

    /*
    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result createFruit(@Valid Fruit fruit){
        fruitList.add(fruit);
        return new Result("Success add data");
    }
    */

    // update a fruit by the index
    @PUT
    @Path("{index}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateFruit(@PathParam("index") Integer index, Fruit f) {
        Fruit fruit = fruitList.get(index);
        fruit.setName(f.getName());
        fruit.setColor(f.getColor());
        fruit.setTotal(f.getTotal());
        return Response.ok(fruit).build();
    }

    // delete a fruit from list by index
    @DELETE
    @Path("{index}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteFruit(@PathParam("index") Integer index) {
        fruitList.remove(index.intValue());
        return Response.ok(fruitList).build();
    }

    /*
    *VALIDATOR
    */
    public static class Result{
        private String message;
        private Boolean success;

        Result(String message){
            this.success = true;
            this.message = message;
        }

        Result(Set<? extends ConstraintViolation<?>> violations) {
            this.success = false;
            this.message = violations.stream()
                    .map(cv -> cv.getMessage())
                    .collect(Collectors.joining(", "));
        }

        public String getMessage() {
            return message;
        }

        public boolean isSuccess(){
            return success;
        }
    }

}
